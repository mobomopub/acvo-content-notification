<?php

namespace Drupal\acvo_content_notification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ResidencyNotification implements settings for Residency notifications.
 */
class ResidencyNotification extends ConfigFormBase {

  /**
   * Get Editable config names.
   *
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['acvo_content_notification.settings'];
  }

  /**
   * Get the form_id.
   *
   * @inheritDoc
   */
  public function getFormId() {
    return 'residency_notification_form';
  }

  /**
   * Build the Form.
   *
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->configFactory->get('acvo_content_notification.settings');
    $form['#tree'] = TRUE;

    $form['residency_notification_content_types'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Select the content types'),
      '#description' => $this->t('Choose the content types that will trigger the Residency program Notification.'),
      '#tree' => TRUE,
    );

    $default_content_types = ($config->get('residency_notification_node_types')) ?: [];
    $form['residency_notification_content_types']['residency_notification_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#default_value' => $default_content_types,
      '#options' => node_type_get_names(),
    ];

    $trigger_node_update = ($config->get('residency_notification_trigger_on_node_update')) ?: FALSE;
    $form['residency_notification_trigger_on_node_update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on update action'),
      '#default_value' => $trigger_node_update,
      '#description' => $this->t('Please check if you want to send notification on update action (node edit) too.'),
    ];

    $form['residency_notification'] = [
      '#type' => 'details',
      '#title' => "Email address(es) to send the notification.",
      '#open' => TRUE,
      '#description' => "Email addresses that will receive the email notification.",
    ];

    $form['residency_notification']['to_emails'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Send notification to:'),
      '#attributes' => [
        'id' => 'emails-fieldset-wrapper',
      ],
      '#tree' => TRUE,
    ];
    $custom_emails = $config->get('residency_notification_emails');
    $default_emails = empty($custom_emails['to_emails']) ? [] : $custom_emails['to_emails'];
    $max = max($form_state->get('fields_count'), count($default_emails), 0);
    $form_state->set('fields_count', $max);
    // Add elements that don't already exist.
    for ($delta = 0; $delta <= $max; $delta++) {
      if (empty($form['residency_notification']['to_emails'][$delta])) {
        $form['residency_notification']['to_emails'][$delta] = [
          '#type' => 'email',
          '#title' => $this->t('Email address'),
          '#default_value' => empty($default_emails[$delta]) ? '' : $default_emails[$delta],
        ];
      }
    }
    $form['residency_notification']['to_emails']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add email address'),
      '#submit' => [[$this, 'addEmailSubmit']],
      '#ajax' => [
        'callback' => [$this, 'addEmailCallback'],
        'wrapper' => 'emails-fieldset-wrapper',
        'effect' => 'fade',
      ],
    ];

    $send_copy_to_author = ($config->get('residency_notification_cc_author')) ?: FALSE;
    $form['residency_notification_cc_author'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send a copy of this email to the node author'),
      '#default_value' => $send_copy_to_author,
      '#description' => $this->t('Please check if you want to send an email notification copy to the node author too.'),
    ];

    $form['residency_notification_email_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email Content'),
      '#tree' => TRUE,
    ];

    $form['residency_notification_email_fieldset']['residency_notification_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Configurable email subject'),
      '#default_value' => $config->get('residency_notification_email_subject'),
      '#description' => $this->t('Enter a default subject of the notification email.'),
    ];

    $form['residency_notification_email_fieldset']['residency_notification_email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Configurable email body'),
      '#default_value' => $config->get('residency_notification_email_body'),
      '#description' => $this->t('Enter the default email template to notify users about new content posted on the site. Use the following tokens: @user_who_posted, @user_last_name, @node_id, @content_link, @content_title, @content_type, @node_date, @action (posted or updated, will update accrodingly).'),
    ];
    $form_state->getValue('residency_notification_email_fieldset');
    return parent::buildForm($form, $form_state);
  }

  /**
   * Add email address submit handler.
   *
   * @param array $form
   *   Settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function addEmailSubmit(array &$form, FormStateInterface &$form_state) {
    $max = $form_state->get('fields_count') + 1;
    $form_state->set('fields_count', $max);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Add email address AJAX handler.
   *
   * @param array $form
   *   Settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Ajax return value.
   */
  public function addEmailCallback(array &$form, FormStateInterface &$form_state) {
    return $form['residency_notification']['to_emails'];
  }

  /**
   * Add submit handler.
   *
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('acvo_content_notification.settings');

    $config->set('residency_notification_node_types', $form_state->getValue(['residency_notification_content_types', 'residency_notification_node_types']));
    $config->set('residency_notification_trigger_on_node_update', $form_state->getValue('residency_notification_trigger_on_node_update'));
    $config->set('residency_notification_cc_author', $form_state->getValue('residency_notification_cc_author'));
    $config->set('residency_notification_email_subject', $form_state->getValue(['residency_notification_email_fieldset', 'residency_notification_email_subject']));
    $config->set('residency_notification_email_body', $form_state->getValue(['residency_notification_email_fieldset', 'residency_notification_email_body']));

    // Add email address from multi value field.
    $notification_emails = $form_state->getValue('residency_notification');
    unset($notification_emails['to_emails']['add']);
    $notification_emails['to_emails'] = array_unique($notification_emails['to_emails']);
    foreach ($notification_emails['to_emails'] as $key => $email_address) {
      if (empty($email_address)) {
        unset($notification_emails['to_emails'][$key]);
      }
    }
    $notification_emails['to_emails'] = array_values($notification_emails['to_emails']);
    $config->set('residency_notification_emails', $notification_emails);

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
