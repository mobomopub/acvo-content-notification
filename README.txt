ACVO Content Notification
------------

Description
-----------

This custom module is heavily based and inspired by Admin Content Notification contrib module.
We have adapted it to fulfill our requirements: use two groups of notifications, based on CT's groupings.


Original module description:
Admin Content Notification is very simple module which 
can be used to track new content posted by a user. 
This can be configured from admin and user can select 
multiple content types for which he wants to receive notification 
on adding a new content. Email's subject and body also can configurable.
 

Install
-------

Copy and paste downloaded module into modules folder.
Enable the module.
Open "/admin/content/credentials-notification" and configure the settings.
